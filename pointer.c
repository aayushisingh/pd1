#include <stdio.h>
void swap(int *c,int*d)
{int temp;
temp=*c;
*c=*d;
*d=temp;
printf("In fun(call by reference) c=%d & d=%d \n",*c,*d);
}
int main()
{int a=1,b=2,c=3,d=4;
printf("in main, a=%d,b=%d \n",a,b);
swap(&c,&d);
printf("in main, c =%d,d=%d \n",c,d);
return 0;
}